#pragma once

#include <SFML/Graphics.hpp>;

/* SFML Color with an initial color and current (temporary) value. Calling "update" will bring the current color closer to the initial color by some step value. */
class FadeColor : public sf::Color
{
public:
	FadeColor();
	FadeColor(sf::Color fadeTo, float fadeSpeed);
	void setFadeFrom(sf::Color fadeFrom);
	void setFadeTo(sf::Color fadeTo);
	void setFadeSpeed(float fadeSpeed);
	void fadeOut();
	bool isFading();
	void update();
	static void updateAll();
private:
	static std::vector<FadeColor*> all;
	float getIncrement(float fromVal, float toVal);
	sf::Color fadeTo;
	sf::Color fadeFrom;
	float fadeSpeed;
};

