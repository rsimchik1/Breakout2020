#include <iostream>
#include "GameView.h";

int main() 
{
    GameView gameView;
    gameView.run();

    return 0;
}