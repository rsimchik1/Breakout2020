#include "FadeColor.h"

std::vector<FadeColor*> FadeColor::all;

FadeColor::FadeColor(sf::Color fadeTo, float fadeSpeed) : sf::Color(fadeTo)
{
	this->fadeTo = fadeTo;
	this->fadeFrom = fadeTo;
	this->fadeSpeed = fadeSpeed;
	all.push_back(this);
}

FadeColor::FadeColor() : FadeColor(sf::Color::Black, 0) {}

void FadeColor::setFadeFrom(sf::Color fadeFrom)
{
	this->fadeFrom = fadeFrom;
	r = fadeFrom.r;
	g = fadeFrom.g;
	b = fadeFrom.b;
	a = fadeFrom.a;
}

void FadeColor::setFadeTo(sf::Color fadeTo)
{
	this->fadeTo = fadeTo;
}

void FadeColor::setFadeSpeed(float fadeSpeed)
{
	this->fadeSpeed = fadeSpeed;
}

void FadeColor::fadeOut()
{
	this->fadeTo = sf::Color(r, g, b, 0);
}

bool FadeColor::isFading()
{
	return fadeFrom != fadeTo;
}

void FadeColor::update()
{
	r += getIncrement(r, fadeTo.r);
	g += getIncrement(g, fadeTo.g);
	b += getIncrement(b, fadeTo.b);
	a += getIncrement(a, fadeTo.a);
}

void FadeColor::updateAll() 
{
	for (int i = 0; i < all.size(); i++)
	{
		if (all[i]->isFading()) all[i]->update();
	}
}

float FadeColor::getIncrement(float fromVal, float toVal)
{
	if (fromVal > toVal)
	{
		if (fromVal - fadeSpeed < toVal) return toVal - fromVal;
		return -fadeSpeed;
	}
	else
	{
		if (fromVal + fadeSpeed > toVal) return toVal - fromVal;
		return fadeSpeed;
	}
}