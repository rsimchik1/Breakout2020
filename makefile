CC = g++
SRC = main.cpp GameView.cpp FadeColor.cpp
LIB = -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio

all: breakout

breakout: $(SRC)
	$(CC) $(SRC) -O3 -o breakout $(LIB)
