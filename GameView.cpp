#include "GameView.h";

#include <iostream>;

GameView::GameView(int width, int height)
{
	this->width = width;
	this->height = height;
	scaleFactor = (width / 1200.f);

	wall.rows = wall.minRows;
	wall.cols = wall.minCols;
	streak = 0;
	maxStreak = 0;
	deaths = 0;
	level = 1;
	backgroundColor = FadeColor(sf::Color(0x000000DD), 1);

	levelColor = wall.colors[0];

	backgroundColor.setFadeTo(sf::Color(levelColor.r / 8, levelColor.g / 8, levelColor.b / 8, backgroundColor.a));

	init();
}

GameView::GameView() : GameView(1500, 1500 * (9.f / 16)) {}


void GameView::init()
{
	wall.bricks = new Brick[wall.rows * wall.cols];

	for (int c = 0; c < wall.cols; c++)
	{
		for (int r = 0; r < wall.rows; r++)
		{
			Brick brick;
			brick.w = width / wall.cols;
			brick.h = (height / 3) / wall.rows;
			brick.x = brick.w * c;
			brick.y = brick.h * r;
			brick.shape = sf::RectangleShape(sf::Vector2f(brick.w - (brick.padding * 2), brick.h - (brick.padding * 2)));
			brick.shape.setPosition(brick.x + brick.padding, brick.y + brick.padding);
			int numColors = sizeof(wall.colors) / sizeof(wall.colors[0]);
			brick.color = FadeColor(wall.colors[r % numColors], 50);
			brick.shape.setFillColor(brick.color);
			wall.bricks[(wall.cols * r) + c] = brick;
		}
	}

	paddle.h = 30 * scaleFactor;
	paddle.w = 150 * scaleFactor;
	paddle.x = width / 2;
	paddle.y = height - (paddle.h / 2);
	paddle.vx = 0;
	paddle.speed = 20 * scaleFactor;
	paddle.shape = sf::RectangleShape(sf::Vector2f(paddle.w, paddle.h));
	paddle.shape.setOrigin(paddle.w / 2, paddle.h / 2);
	paddle.shape.setFillColor(sf::Color::White);

	ball.x = paddle.x;
	ball.y = height - paddle.h;
	ball.speed = 10 * scaleFactor;
	ball.r = 15 * scaleFactor;
	ball.vx = ball.speed;
	ball.vy = -ball.speed;
	ball.shape = sf::CircleShape(ball.r);
	ball.shape.setOrigin(ball.r, ball.r);
	ball.shape.setFillColor(sf::Color::White);

	levelText.setString(std::to_string(level));
	centerText(&levelText);
}

void GameView::lose()
{
	streak = 0;
	level = 1;
	deaths++;
	newRecord = false;
	wall.rows = wall.minRows;
	wall.cols = wall.minCols;

	loseSound.play();

	levelColor = wall.colors[0];
	int alpha = backgroundColor.a;
	backgroundColor.setFadeFrom(levelColor);
	backgroundColor.setFadeTo(sf::Color(levelColor.r / 8, levelColor.g / 8, levelColor.b / 8, alpha));
	backgroundColor.setFadeSpeed(10);

	alpha = levelTextColor.a;
	levelTextColor.setFadeFrom(sf::Color::Black);
	levelTextColor.setFadeTo(sf::Color(levelColor.r / 6, levelColor.g / 6, levelColor.b / 6, alpha));
	levelTextColor.setFadeSpeed(10);

	init();
}

void GameView::win()
{
	winSound.play();
	wall.rows++;
	wall.cols++;

	int numColors = sizeof(wall.colors) / sizeof(wall.colors[0]);
	levelColor = wall.colors[(wall.rows - 1) % numColors];	// Find color of newly added row and apply to background.
	int alpha = backgroundColor.a;
	backgroundColor.setFadeFrom(levelColor);
	backgroundColor.setFadeTo(sf::Color(levelColor.r / 8, levelColor.g / 8, levelColor.b / 8, alpha));
	backgroundColor.setFadeSpeed(5);

	alpha = levelTextColor.a;
	level++;
	levelTextColor.setFadeFrom(sf::Color::White);
	levelTextColor.setFadeTo(sf::Color(levelColor.r / 6, levelColor.g / 6, levelColor.b / 6, alpha));
	levelTextColor.setFadeSpeed(5);

	init();
}

void GameView::score() 
{
	brickSound.play();
	streak++;
	if (streak > maxStreak)
	{
		maxStreak = streak;
		if (!newRecord)
		{
			newRecord = true;
			newRecordSound.play();
		}
	}
}

void GameView::run()
{
	// Set up GUI
	sf::RenderWindow window(sf::VideoMode(width, height), "Breakout");
	window.setFramerateLimit(60);

	sf::RectangleShape background(sf::Vector2f(width, height));
	background.setFillColor(backgroundColor);

	sf::SoundBuffer hitBuffer, brickBuffer, winBuffer, loseBuffer, newRecordBuffer;
	if (!hitBuffer.loadFromFile("./Resources/Audio/Hit.wav")) std::cout << "Failed to load hit audio." << "\n";
	if (!brickBuffer.loadFromFile("./Resources/Audio/Brick.wav")) std::cout << "Failed to load brick audio." << "\n";
	if (!winBuffer.loadFromFile("./Resources/Audio/Win.wav")) std::cout << "Failed to load win audio." << "\n";
	if (!loseBuffer.loadFromFile("./Resources/Audio/Lose.wav")) std::cout << "Failed to load lose audio." << "\n";
	if (!newRecordBuffer.loadFromFile("./Resources/Audio/NewRecord.wav")) std::cout << "Failed to load new record audio." << "\n";
	hitSound.setBuffer(hitBuffer);
	brickSound.setBuffer(brickBuffer);
	winSound.setBuffer(winBuffer);
	loseSound.setBuffer(loseBuffer);
	newRecordSound.setBuffer(newRecordBuffer);
	newRecordSound.setVolume(50);

	sf::Font streakFont;
	if (!streakFont.loadFromFile("./Resources/Instruction.ttf")) std::cout << "Failed to load streak font." << "\n";
	streakText.setFont(streakFont);
	streakText.setFillColor(sf::Color::White);
	streakText.setCharacterSize(30 * scaleFactor);
	streakText.setPosition(sf::Vector2f(0, height - streakText.getCharacterSize()));

	maxStreakText.setFont(streakFont);
	maxStreakText.setFillColor(sf::Color::White);
	maxStreakText.setCharacterSize(30 * scaleFactor);
	maxStreakText.setPosition(sf::Vector2f(0, height - streakText.getCharacterSize() * 2));

	sf::Font levelFont;
	levelTextColor = FadeColor(sf::Color(levelColor.r / 6, levelColor.g / 6, levelColor.b / 6, 255), 0);
	if (!levelFont.loadFromFile("./Resources/OneSlot.ttf")) std::cout << "Failed to load level font." << "\n";
	levelText.setFont(levelFont);
	levelText.setFillColor(levelTextColor);
	levelText.setCharacterSize(500 * scaleFactor);
	levelText.setString(std::to_string(level));
	levelText.setOrigin(levelText.getGlobalBounds().left + levelText.getGlobalBounds().width / 2, levelText.getGlobalBounds().top + levelText.getGlobalBounds().height / 2);
	levelText.setPosition(width / 2, height / 2);
	centerText(&levelText);

	sf::Font pauseFont;
	if (!pauseFont.loadFromFile("./Resources/OneSlot.ttf")) std::cout << "Failed to load pause font." << "\n";
	pauseText.setFont(pauseFont);
	pauseText.setString("PAUSED");
	pauseText.setCharacterSize(200 * scaleFactor);
	pauseText.setFillColor(sf::Color::White);
	pauseText.setOutlineColor(sf::Color::Black);
	pauseText.setOutlineThickness(pauseText.getCharacterSize() / 20);
	pauseText.setOrigin(pauseText.getGlobalBounds().left + pauseText.getGlobalBounds().width / 2, pauseText.getGlobalBounds().top + pauseText.getGlobalBounds().height / 2);
	pauseText.setPosition(width / 2, height / 2);

	// Set input vars
	bool leftDown = false;
	bool rightDown = false;
	bool paused = false;

	// Main game loop
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				window.close();
				break;
			case sf::Event::KeyPressed:
				if (event.key.code == sf::Keyboard::Left || event.key.code == sf::Keyboard::A) leftDown = true;
				else if (event.key.code == sf::Keyboard::Right || event.key.code == sf::Keyboard::D) rightDown = true;
				else if (event.key.code == sf::Keyboard::Space) // Pause
				{
					paused = !paused;
					if (paused)
					{
						window.draw(pauseText);
						window.display();
					}
				}
				break;
			case sf::Event::KeyReleased:
				if (event.key.code == sf::Keyboard::Left || event.key.code == sf::Keyboard::A) leftDown = false;
				else if (event.key.code == sf::Keyboard::Right || event.key.code == sf::Keyboard::D) rightDown = false;
				break;
			}
		}

		if (paused) continue;

		FadeColor::updateAll();
		background.setFillColor(backgroundColor);
		window.draw(background);

		// Update paddle
		paddle.vx = 0;
		if (rightDown) paddle.vx += paddle.speed;
		if (leftDown) paddle.vx -= paddle.speed;
		if (paddle.x + (paddle.w / 2) >= width && paddle.vx > 0) paddle.vx = 0;
		if (paddle.x - (paddle.w / 2) <= 0 && paddle.vx < 0) paddle.vx = 0;
		paddle.x += paddle.vx;

		// Update ball
		ball.x += ball.vx;
		ball.y += ball.vy;
		bool ballHit = false;
		if (ball.y + ball.r >= paddle.y - (paddle.h / 2) &&		// Ball and paddle-top colision
			ball.y < paddle.y - (paddle.h / 2) &&
			ball.x - ball.r <= paddle.x + (paddle.w / 2) &&
			ball.x + ball.r >= paddle.x - (paddle.w / 2))
		{
			ball.vy = -ball.speed;
			ball.vx = (ball.vx / 2) + (paddle.vx / 2) + ball.speed * (ball.x - paddle.x) / 100;
			ballHit = true;
		}
		if (ball.y >= paddle.y - (paddle.h / 2) &&		// Ball and paddle-right collision
			ball.x - ball.r <= paddle.x + (paddle.w / 2) &&
			ball.x > paddle.x)
		{
			ball.vx = (ball.vx / 2) + (paddle.vx / 2) + ball.speed * (ball.x - paddle.x) / 100;
			ballHit = true;
		} else if (ball.y >= paddle.y - (paddle.h / 2) &&		// Ball and paddle-left collision
			ball.x + ball.r >= paddle.x - (paddle.w / 2) &&
			ball.x <= paddle.x)
		{
			ball.vx = (ball.vx / 2) + (paddle.vx / 2) + ball.speed * (ball.x - paddle.x) / 100;
			ballHit = true;
		}
		if (ball.x + ball.r >= width && ball.vx > 0 )	// Ball and right wall collision
		{
			ball.vx = -ball.vx;
			ballHit = true;
		}
		else if (ball.x - ball.r <= 0 && ball.vx < 0)	// Ball and left wall collision
		{
			ball.vx = -ball.vx;
			ballHit = true;
		}
		if (ball.y - ball.r <= 0)	// Ball and top wall collision
		{
			ball.vy = -ball.vy;
			ballHit = true;
		}
		if (ball.y - (ball.r * 2) > height)	lose();		// Ball and floor collision;
		
		if (ballHit) hitSound.play();

		// Draw
		levelText.setFillColor(levelTextColor);
		window.draw(levelText);
		paddle.shape.setPosition(paddle.x, paddle.y);
		window.draw(paddle.shape);
		ball.shape.setPosition(ball.x, ball.y);
		window.draw(ball.shape);
		int visibleBricks = 0;
		for (int c = 0; c < wall.cols; c++)
		{
			for (int r = 0; r < wall.rows; r++)
			{
				Brick* brick = &wall.bricks[(wall.cols * r) + c];
				if (brick->visible)
				{
					bool brickHit = false;
					// Update bricks
					if (ball.vx < 0 &&								// Ball and brick-right collision
						ball.x - ball.r <= brick->x + brick->w &&
						ball.x >= brick->x + (brick->w / 2) &&
						ball.y <= brick->y + brick->h &&
						ball.y >= brick->y)
					{
						brickHit = true;
						ball.vx = -ball.vx;
					}
					else if (ball.vx > 0 &&							// Ball and brick-left collision
						ball.x + ball.r >= brick->x &&
						ball.x <= brick->x + (brick->w / 2) &&
						ball.y <= brick->y + brick->h &&
						ball.y >= brick->y)
					{
						brickHit = true;
						ball.vx = -ball.vx;
					}
					else if (ball.y - ball.r <= brick->y + brick->h &&	// Ball and brick-top-bottom collision
						ball.y + ball.r >= brick->y &&
						ball.x - (ball.r / 2) <= brick->x + brick->w &&
						ball.x + (ball.r / 2) >= brick->x)
					{
						brickHit = true;
						ball.vy = -ball.vy;
					}
					if (brickHit)
					{
						brick->visible = false;
						brick->color.fadeOut();
						score();
					}
					visibleBricks++;
				}
				brick->shape.setFillColor(brick->color);
				window.draw(brick->shape);
			}
		}

		if (visibleBricks == 0) win();

		maxStreakText.setString("Best:  " + std::to_string(maxStreak));
		maxStreakText.setFillColor(sf::Color::White);
		streakText.setString("Score: " + std::to_string(streak));
		streakText.setFillColor(levelColor);
		window.draw(streakText);
		if (deaths > 0 && maxStreak > 0)
		{
			if (newRecord)
			{
				int padding = 3;
				sf::FloatRect boundingRect = maxStreakText.getGlobalBounds();
				sf::RectangleShape maxStreakBackground = sf::RectangleShape(sf::Vector2f(boundingRect.width + padding * 2, boundingRect.height + padding * 2));
				maxStreakBackground.setFillColor(levelColor);
				maxStreakBackground.setPosition(sf::Vector2f(boundingRect.left - padding, boundingRect.top - padding));
				window.draw(maxStreakBackground);
			}
			window.draw(maxStreakText);
		}
		window.display();
	}
}

void GameView::centerText(sf::Text* textToCenter)
{
	textToCenter->setOrigin(0, 0);
	textToCenter->setPosition(0, 0);
	textToCenter->setOrigin(textToCenter->getGlobalBounds().left + textToCenter->getGlobalBounds().width / 2, textToCenter->getGlobalBounds().top + textToCenter->getGlobalBounds().height / 2);
	textToCenter->setPosition(width / 2, height / 2);
}