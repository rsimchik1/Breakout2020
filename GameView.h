#pragma once

#include "SFML/Graphics.hpp";
#include "SFML/Window.hpp";
#include "SFML/Audio.hpp";

#include "FadeColor.h";

struct Ball
{
public:
	float x, y, r, vx, vy, speed;
	sf::CircleShape shape;
	sf::Color color;
};

struct Paddle
{
public:
	float x, y, w, h, vx, speed;
	sf::RectangleShape shape;
	sf::Color color;
};

struct Brick
{
public:
	float x, y, w, h, padding = 1.f;
	bool visible = true;
	sf::RectangleShape shape;
	FadeColor color;
};

struct Wall
{
public:
	int minRows = 1, minCols = 5;
	int rows, cols;
	Brick* bricks = NULL;
	sf::Color colors[10] = { sf::Color(0xE94128FF), sf::Color(0xF18229FF), sf::Color(0xEDD569FF), sf::Color(0x008357FF), sf::Color(0x3F628FFF), sf::Color(0x7258B9FF), sf::Color(0x3F628FFF), sf::Color(0x008357FF), sf::Color(0xEDD569FF), sf::Color(0xF18229FF) };
};

class GameView
{
public:
	GameView();
	GameView(int width, int height);
	void run();
private:
	int streak;
	int maxStreak;
	int width;
	int height;
	int level;
	int deaths;
	float scaleFactor;
	bool newRecord;
	FadeColor backgroundColor;
	FadeColor levelTextColor;
	sf::Color levelColor;
	sf::Text levelText, pauseText, streakText, maxStreakText;
	Ball ball;
	Paddle paddle;
	Wall wall;
	sf::Sound hitSound, brickSound, winSound, loseSound, newRecordSound;
	sf::View gameView;
	void init();
	void win();
	void lose();
	void score();
	void centerText(sf::Text* textToCenter);
	void scale();
};